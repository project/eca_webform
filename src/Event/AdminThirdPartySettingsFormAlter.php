<?php

namespace Drupal\eca_webform\Event;

/**
 * Provides admin third party settings form alter  event for eca_webform.
 *
 * @package Drupal\eca_webform\Event
 */
class AdminThirdPartySettingsFormAlter extends WebformBaseEvent {

}
