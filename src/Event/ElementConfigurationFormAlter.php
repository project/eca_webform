<?php

namespace Drupal\eca_webform\Event;

/**
 * Provides element configuration form alter event for eca_webform.
 *
 * @package Drupal\eca_webform\Event
 */
class ElementConfigurationFormAlter extends WebformBaseEvent {

}
