<?php

namespace Drupal\eca_webform\Event;

/**
 * Provides third party settings form alter event for eca_webform.
 *
 * @package Drupal\eca_webform\Event
 */
class ThirdPartySettingsFormAlter extends WebformBaseEvent {

}
